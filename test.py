# -*- coding: utf-8 -*
import os  

# [Content] XML Footer Text
def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	os.system('adb shell input tap 100 100')

# 1. [Content] Side Bar Text
def test_SidebarOrderText():
	os.system('adb shell input tap 100 100')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('查訂單') != -1

# 2. [Screenshot] Side Bar Text
def test_SidebarScreenshot():
	os.system('adb shell input tap 100 100')
	os.system('adb shell screencap -p /sdcard/sidebar_screen.png && adb pull /sdcard/sidebar_screen.png')

# 3. [Context] Categories
def test_Categories():
	os.system('adb shell input tap 1022 107')
	os.system('adb shell sleep 3')
	os.system('adb shell input swipe 500 1500 300 300')
	os.system('adb shell sleep 3')
	os.system('adb shell input tap 1014 267')
	os.system('adb shell sleep 3')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('精選') != -1

# 4. [Screenshot] Categories
def test_CategoriesScreenshot():
	os.system('adb shell input swipe 500 1500 300 300')
	os.system('adb shell sleep 3')
	os.system('adb shell input tap 1014 267')
	os.system('adb shell sleep 3')
	os.system('adb shell screencap -p /sdcard/category_screen.png && adb pull /sdcard/category_screen.png')

# 5. [Context] Categories page
def test_CategoriesPage():
	os.system('adb shell input tap 378 1692')
	os.system('adb shell sleep 3')
	os.system('adb shell input tap 378 1692')
	os.system('adb shell sleep 3')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('24H購物') != -1

# 6. [Screenshot] Categories page
def test_CategoriesPageScreenshot():
	os.system('adb shell input tap 378 1692')
	os.system('adb shell sleep 3')
	os.system('adb shell screencap -p /sdcard/categorypage_screen.png && adb pull /sdcard/categorypage_screen.png')

# 7. [Behavior] Search item “switch”
def test_Searchswitch():
	os.system('adb shell input tap 276 115')
	os.system('adb shell sleep 3')
	os.system('adb shell input text "switch"')
	os.system('adb shell input keyevent "KEYCODE_ENTER"')
	os.system('adb shell sleep 3')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('Switch') != -1

# 8. [Behavior] Follow an item and it should be add to the list
def test_Checklist():
	os.system('adb shell input tap 598 486')
	os.system('adb shell sleep 3')
	os.system('adb shell input tap 108 1717')
	os.system('adb shell sleep 3')
	os.system('adb shell input tap 981 127')
	os.system('adb shell sleep 3')
	os.system('adb shell input tap 887 395')
	os.system('adb shell sleep 5')
	os.system('adb shell input tap 100 100')
	os.system('adb shell sleep 3')
	os.system('adb shell input tap 297 852')
	os.system('adb shell sleep 5')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('Switch') != -1

# 9. [Behavior] Navigate tto the detail of item
def test_NavigateCheck():
	os.system('adb shell input tap 208 1025')
	os.system('adb shell sleep 3')
	os.system('adb shell input swipe 500 1500 300 300')
	os.system('adb shell sleep 3')
	os.system('adb shell input tap 540 132')
	os.system('adb shell sleep 10')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('商品特色') != -1

# 10. [Screenshot] Disconnetion Screen
def test_DisconnectScreenshot():
	os.system('adb shell svc wifi disable')
	os.system('adb shell svc data disable')
	os.system('adb shell input tap 981 127')
	os.system('adb shell sleep 3')
	os.system('adb shell input tap 887 395')
	os.system('adb shell sleep 5')
	os.system('adb shell screencap -p /sdcard/disconnect_screen.png && adb pull /sdcard/disconnect_screen.png')
